﻿# Asteroids via DOTS

## How to Play

W / UpArrow - Thruster;
A / Left Arrow - Turn Left;
D / Right Arrow - Turn Right;
Space / Left Mouse - Shoot;
LCTRL / Right Mouse - HyperSpace Jump

## Powerups (visual guide available in Documentation)
1- Twin Shot
2 - Rapid Fire
3 - Invunerability Shield
4 - Helper Drone

To Play:
Open "SampleScene" located in the Scene folder and enable the child "EntitySubScene"

On lower end machines a first time Play results in stuttering for approx 10 seconds while all the necessary Unity Resources are allocated.
Subsequent playthroughs dont suffer from this issue. 


