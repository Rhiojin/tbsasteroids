using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;

public class UfoAiSystem : SystemBase
{
	float _timeToThink = 8;
	float _thinkTimer;

	protected override void OnUpdate()
	{
		float deltaTime = Time.DeltaTime;
		bool rethink = _thinkTimer <= 0;
		var randomArray = World.GetExistingSystem<RandomSystem>().RandomArray;

		if (rethink)
		{
			_thinkTimer = _timeToThink;
		}

		Entities
			.WithNativeDisableParallelForRestriction(randomArray)
			.ForEach((int nativeThreadIndex, ref Translation translation, ref UfoData ufoData ) =>
		{
			var rand = randomArray[nativeThreadIndex];
			if (rethink)
			{
				// pick new random direction to travel;
				ufoData.Direction = new float3(rand.NextFloat(-1, 1), rand.NextFloat(-1, 1), 0);
			}

			translation.Value += ufoData.Direction * deltaTime * ufoData.Speed;
			randomArray[nativeThreadIndex] = rand;

		}).ScheduleParallel();

		_thinkTimer -= deltaTime;
	}
}
