using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

public class HyperSpaceSystem : JobComponentSystem
{
	Random rand;
	protected override void OnCreate()
	{
		base.OnCreate();
		rand = new Random(10);
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		Entities.ForEach((ref Translation translation,ref ShipMovementData moveData) =>
		{
			if(moveData.HyperSpaceActive)
			{
				moveData.HyperSpaceActive = false;
				Vector2 _botLeft = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
				Vector2 _topRight = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

				float xPos = rand.NextFloat(_botLeft.x, _topRight.x);
				float yPos = rand.NextFloat(_botLeft.y, _topRight.y);

				translation.Value = new float3(xPos, yPos, 0);
			}

		}).WithStructuralChanges().Run();

		return default;
	}
}
