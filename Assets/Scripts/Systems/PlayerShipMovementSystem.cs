using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics;

public class PlayerShipMovementSystem : JobComponentSystem
{

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		float deltaTime = Time.DeltaTime;

		var job = Entities.ForEach((ref PhysicsVelocity vel, ref Rotation rotation, in ShipMovementData movementData, in LocalToWorld ltw) =>
		{
			if(movementData.RotateLeft)
			{
				rotation.Value = math.mul(rotation.Value, quaternion.RotateZ(math.radians(movementData.TurnSpeed * deltaTime)));
			}

			if(movementData.RotateRight)
			{
				rotation.Value = math.mul(rotation.Value, quaternion.RotateZ(math.radians(-movementData.TurnSpeed * deltaTime)));

			}

			if (movementData.ThrusterOn)
			{
				vel.Linear += ltw.Up * movementData.Acceleration * deltaTime;
			}

		}).Schedule(inputDeps);

		return job;
	}
}
 