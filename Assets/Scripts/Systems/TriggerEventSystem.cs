
using Unity.Jobs;
using Unity.Entities;
using Unity.Physics;
using Unity.Burst;
using Unity.Physics.Systems;

public class TriggerEventSystem : JobComponentSystem
{
	
	BuildPhysicsWorld _physicsWorld;
	StepPhysicsWorld _setpPhysicsWorld;

	[BurstCompile]
	public struct TriggerJob : ITriggerEventsJob
	{
		public ComponentDataFromEntity<PowerupData> PowerupGroup;
		public ComponentDataFromEntity<ShipMovementData> PlayerShipGroup;
		public ComponentDataFromEntity<HealthData> HealthGroup;
		public ComponentDataFromEntity<PlayerPowerupStateData> PowerupStateGroup;

		public void Execute(TriggerEvent triggerEvent)
		{

			Entity entA = triggerEvent.EntityA;
			Entity entB = triggerEvent.EntityB;

			bool entAisShip = PlayerShipGroup.HasComponent(entA);
			bool entBisShip = PlayerShipGroup.HasComponent(entB);

			if (entAisShip || entBisShip)
			{
				
				Entity powerupEntity = entAisShip ? entB : entA;
				Entity shipEntity = entAisShip ? entA : entB;

				//set powerup for cleanup
				if (HealthGroup.HasComponent(powerupEntity))
				{
					HealthData newHealh =  HealthGroup[powerupEntity];
					newHealh.Health--;
					HealthGroup[powerupEntity] = newHealh;
				}

				if (PowerupGroup.HasComponent(powerupEntity))
				{
					int powerupType = PowerupGroup[powerupEntity].PowerupType;

					switch (powerupType)
					{
						case 0:
							{
								PlayerPowerupStateData newPowerState = PowerupStateGroup[shipEntity];
								newPowerState.TwinShot = true;
								newPowerState.TwinShotTimer = 10;
								PowerupStateGroup[shipEntity] = newPowerState;
							}
							break;

						case 1:
							{
								PlayerPowerupStateData newPowerState = PowerupStateGroup[shipEntity];
								newPowerState.Shield = true;
								newPowerState.ShieldTimer = 5;
								PowerupStateGroup[shipEntity] = newPowerState;
							}
							break;

						case 2:
							{
								PlayerPowerupStateData newPowerState = PowerupStateGroup[shipEntity];
								newPowerState.Drone = true;
								newPowerState.DroneTimer = 15;
								PowerupStateGroup[shipEntity] = newPowerState;
							}
							break;

						case 3:
							{
								PlayerPowerupStateData newPowerState = PowerupStateGroup[shipEntity];
								newPowerState.RapidFire = true;
								newPowerState.RapidFireTimer = 8;
								PowerupStateGroup[shipEntity] = newPowerState;
							}
							break;

					}
				}
			}
		}
	}

	protected override void OnCreate()
	{
		_physicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
		_setpPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		TriggerJob triggerJob = new TriggerJob();
		triggerJob.PowerupGroup = GetComponentDataFromEntity<PowerupData>(false);
		triggerJob.PlayerShipGroup = GetComponentDataFromEntity<ShipMovementData>(false);
		triggerJob.HealthGroup = GetComponentDataFromEntity<HealthData>(false);
		triggerJob.PowerupStateGroup = GetComponentDataFromEntity<PlayerPowerupStateData>(false);

		return triggerJob.Schedule(_setpPhysicsWorld.Simulation, ref _physicsWorld.PhysicsWorld, inputDeps);
	}
}
