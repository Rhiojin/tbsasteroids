using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Transforms;

public class ShieldManagementSystem : SystemBase
{

	protected override void OnUpdate()
	{
		NativeArray<float3> playerPositon = new NativeArray<float3>(1, Allocator.TempJob);

		//save player position
		Entities.ForEach((int entityInQueryIndex, ref Translation translation, in ShipMovementData shipData) =>
		{
			playerPositon[0] = translation.Value;

		}).ScheduleParallel();

		//apply player position
		Entities.ForEach((ref Translation shieldTranslation, in ShieldTag tag) =>
		{
			shieldTranslation.Value = playerPositon[0];

		}).WithDisposeOnCompletion(playerPositon).ScheduleParallel();

	}
}
