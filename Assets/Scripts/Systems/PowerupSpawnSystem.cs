using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

[AlwaysSynchronizeSystem]
public class PowerupSpawnSystem : SystemBase
{
	EntityQuery _query;
	float _spawnTimer = 5;
	float _spawnInterval = 5f;
	int _powerupCount;
	int _maxPowerups = 1;

	Random rand;


	protected override void OnCreate()
	{
		base.OnCreate();
		rand = new Random(69);
	}

	protected override void OnUpdate()
	{
		_spawnTimer -= Time.DeltaTime;

		EntityQueryDesc description = new EntityQueryDesc
		{
			All = new ComponentType[]
			{
			   ComponentType.ReadOnly<PowerupEffectData>()
			}
		};
		_query = GetEntityQuery(description);
		NativeArray<PowerupEffectData> powerupEffectDataArray = new NativeArray<PowerupEffectData>(1, Allocator.TempJob);

		//spawn powerups on timer
		if (_spawnTimer <= 0 && _powerupCount < _maxPowerups)
		{
			_powerupCount++;
			Vector2 _botLeft = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
			Vector2 _topRight = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
			float edgeBuffer = 2;

			float xPos = rand.NextFloat(_botLeft.x + edgeBuffer, _topRight.x - edgeBuffer);
			float yPos = rand.NextFloat(_botLeft.y + edgeBuffer, _topRight.y - edgeBuffer);

			Entities.ForEach((in PowerupSpawnData powerupSpawnData) =>
			{
				int randPow = rand.NextInt(0, 4);
				Entity pow = new Entity();
				switch (randPow)
				{
					case 0:
						pow = EntityManager.Instantiate(powerupSpawnData.TwinShot);
						break;
					case 1:
						pow = EntityManager.Instantiate(powerupSpawnData.Shield);
						break;
					case 2:
						pow = EntityManager.Instantiate(powerupSpawnData.Drone);
						break;
					case 3:
						pow = EntityManager.Instantiate(powerupSpawnData.RapidFire);
						break;
				}

				EntityManager.SetComponentData(pow, new Translation { Value = new float3(xPos, yPos, 0) });

			}).WithStructuralChanges().Run();

		}

		//grab powerupEffectData
		Entities
			.WithStoreEntityQueryInField(ref _query)
			.ForEach((ref PowerupEffectData effectData) =>
			{
				powerupEffectDataArray[0] = effectData;

			}).ScheduleParallel();

		//cleanup powerup, spawn associated Effect
		Entities.ForEach((ref Entity entity, ref HealthData health, in PowerupData powerupData) =>
		{
			if (health.Health <= 0)
			{
				//shield
				if (powerupData.PowerupType == 1)
				{
					EntityManager.Instantiate(powerupEffectDataArray[0].ShieldEffect);
				}

				//drone
				if (powerupData.PowerupType == 2)
				{
					EntityManager.Instantiate(powerupEffectDataArray[0].Drone);
				}

				_spawnTimer = _spawnInterval;
				_powerupCount--;
				EntityManager.DestroyEntity(entity);
			}

		}).WithDisposeOnCompletion(powerupEffectDataArray).WithStructuralChanges().Run();
	}
}
