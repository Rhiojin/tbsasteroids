using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[AlwaysSynchronizeSystem]
public class PlayerShipCreator : JobComponentSystem
{
	int _shipCount;

	float _spawnTimer;
	float _spawnInterval = 2f;


	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		if (_spawnTimer > 0)
		{
			_spawnTimer -= Time.DeltaTime;
		}

		if (_spawnTimer <= 0 && _shipCount < 1)
		{
			_shipCount++;
			Entities.ForEach((in PlayerShipSpawnData shipData) =>
			{
				Entity ship = EntityManager.Instantiate(shipData.NormalShip);
				EntityManager.SetComponentData(ship, new Translation { Value = new float3(0, 0, 0) });
			}).WithStructuralChanges().Run();
		}

		Entities.ForEach((int entityInQueryIndex, ref Entity entity, ref HealthData health, in ShipMovementData powerupData, in PlayerPowerupStateData powerupState) =>
		{
			//invunerability powerup
			if (powerupState.Shield)
			{
				health.Health = 1;
			}

			if (health.Health <= 0)
			{
				_spawnTimer = _spawnInterval;
				_shipCount--;
				EntityManager.DestroyEntity(entity);
			}

		}).WithStructuralChanges().Run();

		return default;
	}
}