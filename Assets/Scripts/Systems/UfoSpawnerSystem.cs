using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

[AlwaysSynchronizeSystem]
public class UfoSpawnerSystem : JobComponentSystem
{
	float _spawnTimer = 5;
	float _spawnInterval = 5f;
	int _ufoCount;
	int _maxUfos = 1;

	Random rand;

	protected override void OnCreate()
	{
		base.OnCreate();
		rand = new Random(15);
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var deltaTime = Time.DeltaTime;
		_spawnTimer -= deltaTime;

		if (_spawnTimer <= 0 && _ufoCount < _maxUfos)
		{
			_ufoCount++;
			_spawnTimer = _spawnInterval;

			Vector2 _botLeft = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
			Vector2 _topRight = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

			float xPos = rand.NextFloat(_botLeft.x, _topRight.x);
			float yPos = rand.NextFloat(_botLeft.y, _topRight.y);

			bool coinflip = rand.NextBool();
			float3 spawnPoint = new float3(coinflip ? _botLeft.x : _topRight.x, coinflip ? _botLeft.y : _topRight.y, 0);

			Entities.ForEach((in UfoSpawnData ufoData) =>
			{
				Entity ufo = EntityManager.Instantiate(ufoData.BasicUfo);
				EntityManager.SetComponentData(ufo,	new Translation { Value = spawnPoint });

			}).WithStructuralChanges().Run();
		}

		Entities.ForEach((int entityInQueryIndex, ref Entity entity, ref HealthData health,  in UfoData ufoData) =>
		{
			if (health.Health <= 0)
			{
				_spawnTimer = _spawnInterval;
				_ufoCount--;
				EntityManager.DestroyEntity(entity);
			}

		}).WithStructuralChanges().Run();

		return default;
	}
}