using Unity.Entities;
using Unity.Jobs;

public class ProjectitleCleanupSystem : SystemBase
{
	EndSimulationEntityCommandBufferSystem _endSimulationEcbSystem;
	protected override void OnCreate()
	{
		base.OnCreate();
		_endSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
	}

	protected override void OnUpdate()
	{
		var ecb = _endSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
		var deltaTime = Time.DeltaTime;

		Entities.ForEach((int entityInQueryIndex, ref Entity entity, ref ProjectileData projectileData, in HealthData health) =>
			{
				projectileData.TimeToLive -= deltaTime;
				if(projectileData.TimeToLive <= 0)
				{
					ecb.DestroyEntity(entityInQueryIndex, entity);
				}

				else if (health.Health <= 0)
				{
					ecb.DestroyEntity(entityInQueryIndex, entity);
				}

			}).ScheduleParallel();

		_endSimulationEcbSystem.AddJobHandleForProducer(Dependency);
	}
}
