using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

[AlwaysSynchronizeSystem]
public class AsteroidSpawnerSystem : JobComponentSystem
{
	EntityQuery _query;

	float _spawnTimer;
	float _spawnInterval = 0.5f;
	int _asteroidCount;
	int _triggerCount;
	int _maxAsteroids = 5;

	bool _spawnMediumAsteroids;
	bool _spawnSmallAsteroids;
	float3 _mediumSpawnPoint;
	float3 _smallSpawnPoint;

	Random rand;

	protected override void OnCreate()
	{
		base.OnCreate();
		rand = new Random(2);
	}

	//handles the cleanup setup for asteroids to facilitate spawning new ones

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var deltaTime = Time.DeltaTime;
		_spawnTimer -= deltaTime;

		
		if (_spawnTimer <= 0 && _asteroidCount < _maxAsteroids)
		{
			_asteroidCount++;
			_spawnTimer = _spawnInterval;

			Vector2 _botLeft = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
			Vector2 _topRight = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

			float xPos = rand.NextFloat(_botLeft.x, _topRight.x);
			float yPos = rand.NextFloat(_botLeft.y, _topRight.y);

			Entities.ForEach((in AsteroidSpawnData asteroidData) =>
			{
				Entity asteroid = EntityManager.Instantiate(asteroidData.LargeAsteroid);
				EntityManager.SetComponentData(asteroid,
					new Translation { Value = new float3(xPos, yPos, 0 )});

			}).WithStructuralChanges().Run();

		}

		Entities.ForEach((in AsteroidSpawnData asteroidData) =>
		{
			if (_spawnMediumAsteroids)
			{
				_spawnMediumAsteroids = false;
				Entity mediumAsteroid = EntityManager.Instantiate(asteroidData.MediumAsteroid);
				EntityManager.SetComponentData(mediumAsteroid,
					new Translation { Value = _mediumSpawnPoint });

				_spawnMediumAsteroids = false;
				Entity mediumAsteroid2 = EntityManager.Instantiate(asteroidData.MediumAsteroid);
				EntityManager.SetComponentData(mediumAsteroid2,
					new Translation { Value = _mediumSpawnPoint });
			}

			if (_spawnSmallAsteroids)
			{
				_spawnSmallAsteroids = false;
				Entity smallAsteroid = EntityManager.Instantiate(asteroidData.SmallAsteroid);
				EntityManager.SetComponentData(smallAsteroid,
					new Translation { Value = _smallSpawnPoint });

				_spawnSmallAsteroids = false;
				Entity smallAsteroid2 = EntityManager.Instantiate(asteroidData.SmallAsteroid);
				EntityManager.SetComponentData(smallAsteroid2,
					new Translation { Value = _smallSpawnPoint });
			}

		}).WithStructuralChanges().Run();

	
		//asteroid destroy job. spawnScoreCounters
		Entities.ForEach((int entityInQueryIndex, ref Entity entity, ref HealthData health, in Translation translation, in AsteroidTag asteroidTag) =>
		{
			//for spawning new asteroids we bank on the fact that only 1  asteroid and bullets destruction should happen per loop
			if (health.Health <= 0)
			{
				EntityArchetype archetype = EntityManager.CreateArchetype(typeof(ScoreCounterTag));
				Entity e = EntityManager.CreateEntity(archetype);
				EntityManager.Instantiate(e);

				int asteroidType = asteroidTag.AsteroidType;
				switch (asteroidType)
				{
					case 0:
						{
							_spawnMediumAsteroids = true;
							_mediumSpawnPoint = translation.Value;
							//only count big asteroids to create wave gameplay
							_triggerCount++;
							if (_triggerCount >= _maxAsteroids)
							{
								_asteroidCount = 0;
								_triggerCount = 0;
							}
						}
						break;

					case 1:
						{
							_spawnSmallAsteroids = true;
							_smallSpawnPoint = translation.Value;
						}
						break;
				}

				EntityManager.DestroyEntity(entity);

			}

		}).WithStructuralChanges().Run();

		EntityQueryDesc description = new EntityQueryDesc
		{
			All = new ComponentType[]
			{
			   ComponentType.ReadOnly<ScoreCounterTag>()
			}
		};
		_query = GetEntityQuery(description);
		var useQuery = _query;

		Entities.ForEach((ref ScoreData scoreData) =>
		{
			int count = useQuery.CalculateEntityCount();
			scoreData.Score += count;
			

		}).Run();

		Entities.ForEach((ref Entity scoreEntity, in ScoreCounterTag tag) =>
		{
			EntityManager.DestroyEntity(scoreEntity);

		}).WithStructuralChanges().Run();


		return default;
	}
}
