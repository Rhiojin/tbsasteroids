using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;

public class ProjectileSystem : JobComponentSystem
{
	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		float deltaTime = Time.DeltaTime;

		var job = Entities.ForEach((ref Translation translation, in ProjectileData projectileData) =>
		{
			translation.Value.x = translation.Value.x + (projectileData.Speed * projectileData.Direction.x * deltaTime);
			translation.Value.y = translation.Value.y + (projectileData.Speed * projectileData.Direction.y * deltaTime);

		}).Schedule(inputDeps);

		return job;
	}
}
