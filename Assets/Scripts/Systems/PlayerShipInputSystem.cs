using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

[AlwaysSynchronizeSystem]
public class PlayerShipInputSystem : JobComponentSystem
{
	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		float deltaTime = Time.DeltaTime;

		Entities.ForEach((ref ShipMovementData moveData,
			ref ShipInputData inputData,
			in Translation translation,
			in Rotation rotation,
			in ProjectileTypesData projectiles,
			in PlayerPowerupStateData powerupState,
			in LocalToWorld ltw) =>
		{

			if(Input.GetKeyDown(inputData.LeftKey) || Input.GetKeyDown(inputData.LeftKeyAlt))
			{
				moveData.RotateLeft = true;
			}
			if (Input.GetKeyUp(inputData.LeftKey) || Input.GetKeyUp(inputData.LeftKeyAlt))
			{
				moveData.RotateLeft = false;
			}

			if (Input.GetKeyDown(inputData.RightKey) || Input.GetKeyDown(inputData.RightKeyAlt))
			{
				moveData.RotateRight = true;
			}
			if (Input.GetKeyUp(inputData.RightKey) || Input.GetKeyUp(inputData.RightKeyAlt))
			{
				moveData.RotateRight = false;
			}

			if (Input.GetKeyDown(inputData.ThrustKey) || Input.GetKeyDown(inputData.ThrustKeyAlt))
			{
				moveData.ThrusterOn = true;
			}

			if (Input.GetKeyUp(inputData.ThrustKey) || Input.GetKeyUp(inputData.ThrustKeyAlt))
			{
				moveData.ThrusterOn = false;
			}

			if (Input.GetKeyDown(inputData.FireKey) || Input.GetKeyDown(inputData.FireKeyAlt))
			{
				
				if (inputData.FireSpeed <= 0)
				{
					if (powerupState.RapidFire)
					{
						inputData.FireSpeed = 0.05f;
					}
					else
					{
						inputData.FireSpeed = 0.25f;
					}

					Entity projectile;
					if (powerupState.TwinShot)
					{
						projectile = EntityManager.Instantiate(projectiles.BasicProjectile);
						EntityManager.SetComponentData(projectile, new Translation { Value = translation.Value + ltw.Up + (ltw.Right/2) });
						EntityManager.SetComponentData(projectile, new Rotation { Value = rotation.Value });
						EntityManager.SetComponentData(projectile, new ProjectileData { Speed = 30, Direction = ltw.Up, TimeToLive = 0.5f });

						projectile = EntityManager.Instantiate(projectiles.BasicProjectile);
						EntityManager.SetComponentData(projectile, new Translation { Value = translation.Value + ltw.Up - (ltw.Right / 2) });
						EntityManager.SetComponentData(projectile, new Rotation { Value = rotation.Value });
						EntityManager.SetComponentData(projectile, new ProjectileData { Speed = 30, Direction = ltw.Up, TimeToLive = 0.5f });
					}
					else
					{
						if(powerupState.RapidFire)
							projectile = EntityManager.Instantiate(projectiles.RapidFireProjectile);
						else
							projectile = EntityManager.Instantiate(projectiles.BasicProjectile);

						EntityManager.SetComponentData(projectile, new Translation { Value = translation.Value + ltw.Up });
						EntityManager.SetComponentData(projectile, new Rotation { Value = rotation.Value });
						EntityManager.SetComponentData(projectile, new ProjectileData { Speed = 30, Direction = ltw.Up, TimeToLive = 0.5f });
					}
				}
			}

			if (Input.GetKeyDown(inputData.HyperSpaceKey) || Input.GetKeyDown(inputData.HyperSpaceKeyAlt))
			{
				moveData.HyperSpaceActive = true;
			}

			if (inputData.FireSpeed > 0)
				inputData.FireSpeed -= deltaTime;

		}).WithStructuralChanges().Run();

		return default;
	}
}