using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Transforms;

public class DroneAiSystem : SystemBase
{
	float _fireTimer;
	float _fireInterval = 0.8f;

	protected override void OnUpdate()
	{
		float deltaTime = Time.DeltaTime;
		_fireTimer -= deltaTime;

		NativeArray<float3> playerPositon = new NativeArray<float3>(1, Allocator.TempJob);
		//NativeArray<quaternion> playerRotation = new NativeArray<quaternion>(1, Allocator.TempJob);

		//save player position
		Entities.ForEach((int entityInQueryIndex, in Translation translation,in ShipMovementData shipData, in LocalToWorld ltw) =>
		{
			playerPositon[0] = translation.Value - ltw.Up;

		}).Schedule();

		//save player rotation
		//Entities.ForEach((int entityInQueryIndex, in Rotation rotation, in ShipMovementData shipData, in LocalToWorld ltw) =>
		//{
		//	playerRotation[0] = rotation.Value;

		//}).ScheduleParallel();

		//apply player position to drone
		//when the player dies this causes an index exception. The length checks dont seem to solve it.
		Entities.ForEach((ref Translation droneTranslation, in DroneTag tag) =>
		{
			if(playerPositon.Length > 0)
			{
				float3 val = playerPositon[0];
				droneTranslation.Value = val;
			}
				

		}).WithDisposeOnCompletion(playerPositon).ScheduleParallel();

		//apply player position to drone projectiles
		//quaternion val = playerRotation[0];
		//Entities.ForEach((ref Rotation rotation, in DroneProjectileTag tag) =>
		//{
		//	if (playerRotation.Length > 0)
		//	{
		//		rotation.Value = val;
		//	}

		//}).ScheduleParallel();

		Entities.ForEach((ref Translation translation, ref Rotation rotation, in ProjectileData projectileData, in DroneProjectileTag tag) =>
		{
			translation.Value.x = translation.Value.x + (projectileData.Speed * projectileData.Direction.x * deltaTime);
			translation.Value.y = translation.Value.y + (projectileData.Speed * projectileData.Direction.y * deltaTime);

		}).Schedule();

		if (_fireTimer <= 0)
		{
			_fireTimer = _fireInterval;
			Entities.ForEach((ref Translation droneTranslation, in DroneTag tag, in DroneProjectileTypeData projectile, in LocalToWorld ltw) =>
			{
				Entity proj = EntityManager.Instantiate(projectile.DroneProjectile);
				EntityManager.SetComponentData(proj, new Translation { Value = droneTranslation.Value - ltw.Up });
				//EntityManager.SetComponentData(proj, new Rotation { Value = val });
				EntityManager.SetComponentData(proj, new ProjectileData { Speed = 20, Direction = -ltw.Up, TimeToLive = 0.5f });

			}).WithStructuralChanges().Run();
		}

		//playerRotation.Dispose();
	}
}