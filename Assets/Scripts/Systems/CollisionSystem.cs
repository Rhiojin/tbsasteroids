using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

public class CollisionSystem : JobComponentSystem
{
	private BuildPhysicsWorld buildPhysicsWorldSystem;
	private StepPhysicsWorld stepPhysicsWorldSystem;
	private EndSimulationEntityCommandBufferSystem commandBufferSystem;


	[BurstCompile]
	private struct CollisionJob : ICollisionEventsJob
	{
		//several of these should be readonly per standards but syntax seems to have changes somewhere
		public ComponentDataFromEntity<AsteroidTag> AsteroidGroup;
		public ComponentDataFromEntity<UfoData> UfoGroup;
		public ComponentDataFromEntity<ProjectileData> ProjectileGroup;
		public ComponentDataFromEntity<ShipMovementData> Ship;
		public ComponentDataFromEntity<HealthData> healthGroup;

		public void Execute(CollisionEvent collisionEvent)
		{
			Entity entA = collisionEvent.EntityA;
			Entity entB = collisionEvent.EntityB;
			
			bool entAIsShip = Ship.HasComponent(entA);
			bool entBIsShip = Ship.HasComponent(entB);

			bool entAIsProjectile = ProjectileGroup.HasComponent(entA);
			bool entBIsProjectile = ProjectileGroup.HasComponent(entB);

			bool entAisAsteroid = AsteroidGroup.HasComponent(entA);
			bool entBisAsteroid = AsteroidGroup.HasComponent(entB);

			bool entAisUfo = UfoGroup.HasComponent(entA);
			bool entBisUfo = UfoGroup.HasComponent(entB);

			//Handle Ship to asteroid or ufo collision
			if (entAIsShip || entBIsShip)
			{
				if(entAisAsteroid || entBisAsteroid || entAisUfo || entBisUfo)
				{
					HealthData newHealh1 = healthGroup[entA];
					newHealh1.Health--;
					healthGroup[entA] = newHealh1;

					HealthData newHealh2 = healthGroup[entB];
					newHealh2.Health--;
					healthGroup[entB] = newHealh2;
				}
			}

			//all colliders have 1 hp so we can take a few liberties

			//handle projectile to asteroid Collision
			if (entAIsProjectile || entBIsProjectile)
			{
				if (entAisAsteroid || entBisAsteroid)
				{
					HealthData newHealh1 = healthGroup[entA];
					newHealh1.Health--;
					healthGroup[entA] = newHealh1;

					HealthData newHealh2 = healthGroup[entB];
					newHealh2.Health--;
					healthGroup[entB] = newHealh2;
				}
			}

			//handle projectile to ufo Collision
			if (entAIsProjectile || entBIsProjectile)
			{
				if (entAisUfo || entBisUfo)
				{
					HealthData newHealh1 = healthGroup[entA];
					newHealh1.Health--;
					healthGroup[entA] = newHealh1;

					HealthData newHealh2 = healthGroup[entB];
					newHealh2.Health--;
					healthGroup[entB] = newHealh2;
				}
			}
		}
	}

	protected override void OnCreate()
	{
		base.OnCreate();
		buildPhysicsWorldSystem = World.GetExistingSystem<BuildPhysicsWorld>();
		stepPhysicsWorldSystem = World.GetExistingSystem<StepPhysicsWorld>();
		commandBufferSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();

	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{

		var job = new CollisionJob();
		
		job.AsteroidGroup = GetComponentDataFromEntity<AsteroidTag>(false);
		job.ProjectileGroup = GetComponentDataFromEntity<ProjectileData>(false);
		job.Ship = GetComponentDataFromEntity<ShipMovementData>(false);
		job.healthGroup = GetComponentDataFromEntity<HealthData>(false);
		job.UfoGroup = GetComponentDataFromEntity<UfoData>(false);

		JobHandle jobHandle = job.Schedule(
			stepPhysicsWorldSystem.Simulation,
			ref buildPhysicsWorldSystem.PhysicsWorld,
			inputDeps);

		commandBufferSystem.AddJobHandleForProducer(jobHandle);

		return jobHandle;
	}
}
