using Unity.Entities;
using Unity.Transforms;
using Unity.Jobs;
using UnityEngine;

[AlwaysSynchronizeSystem]
public class WorldWrapSystem : JobComponentSystem
{
	
	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		Vector2 _botLeft = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		Vector2 _topRight = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		float _edgeBuffer = 0.1f;

		var job = Entities.WithAny<WorldWrapTag>().ForEach((ref Translation translation) =>
		{
			if(translation.Value.x > _topRight.x)
			{
				translation.Value.x = _botLeft.x + _edgeBuffer;
			}

			if (translation.Value.x < _botLeft.x)
			{
				translation.Value.x = _topRight.x - _edgeBuffer;
			}

			if (translation.Value.y > _topRight.y)
			{
				translation.Value.y = _botLeft.y + _edgeBuffer;
			}

			if (translation.Value.y < _botLeft.y)
			{
				translation.Value.y = _topRight.y - _edgeBuffer;
			}

		}).Schedule(inputDeps);

		return job;
	}
}
