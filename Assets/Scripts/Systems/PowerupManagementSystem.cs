using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;

public class PowerupManagementSystem : SystemBase
{
	protected override void OnUpdate()
	{
		float deltaTime = Time.DeltaTime;

		NativeArray<PlayerPowerupStateData> powerupStateArray = new NativeArray<PlayerPowerupStateData>(1, Allocator.TempJob);

		Entities.ForEach((ref PlayerPowerupStateData powerupState) =>
		{
			if(powerupState.TwinShotTimer > 0)
			{
				powerupState.TwinShotTimer -= deltaTime;
				if(powerupState.TwinShotTimer <= 0)
				{
					powerupState.TwinShot = false;
				}
			}

			if(powerupState.ShieldTimer > 0)
			{
				powerupState.ShieldTimer -= deltaTime;
				if(powerupState.ShieldTimer <= 0)
				{
					powerupState.Shield = false;
					powerupState.FlagShieldForCleanup = true;
				}
			}

			if(powerupState.DroneTimer > 0)
			{
				powerupState.DroneTimer -= deltaTime;
				if(powerupState.DroneTimer <= 0)
				{
					powerupState.Drone = false;
					powerupState.FlagDroneForCleanup = true;
				}
			}

			if(powerupState.RapidFireTimer > 0)
			{
				powerupState.RapidFireTimer -= deltaTime;
				if(powerupState.RapidFireTimer <= 0)
				{
					powerupState.RapidFire = false;
				}
			}

			powerupStateArray[0] = powerupState;

		}).ScheduleParallel();

		//clean up shield
		Entities.ForEach((ref Entity entity,in ShieldTag tag) =>
		{
			if(powerupStateArray[0].FlagShieldForCleanup)
			{
				EntityManager.DestroyEntity(entity);
				PlayerPowerupStateData powerupState = powerupStateArray[0];
				powerupState.FlagShieldForCleanup = false;
				powerupStateArray[0] = powerupState;
			}

		}).WithStructuralChanges().Run();

		//clean up drone
		Entities.ForEach((ref Entity entity, in DroneTag tag) =>
		{
			if (powerupStateArray[0].FlagDroneForCleanup)
			{
				EntityManager.DestroyEntity(entity);
				PlayerPowerupStateData powerupState = powerupStateArray[0];
				powerupState.FlagDroneForCleanup = false;
				powerupStateArray[0] = powerupState;
			}

		}).WithStructuralChanges().Run();

		//apply new state
		//i think this part is causing some timeing issue with powerups but we are out of time.
		Entities.ForEach((ref PlayerPowerupStateData powerupState) =>
		{
			powerupState = powerupStateArray[0];
		}).Run();

		powerupStateArray.Dispose();
	}
}
