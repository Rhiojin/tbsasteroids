using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;

public class AsteroidInitSystem : SystemBase
{
	EndSimulationEntityCommandBufferSystem _endSimulationEcbSystem;
	protected override void OnCreate()
	{
		base.OnCreate();
		_endSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
	}

	protected override void OnUpdate()
	{
		var randomArray = World.GetExistingSystem<RandomSystem>().RandomArray;
		var ecb = _endSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();

		Entities
			.WithNativeDisableParallelForRestriction(randomArray)
			.ForEach((int nativeThreadIndex, int entityInQueryIndex, Entity entity, ref PhysicsVelocity vel, ref AsteroidInitTag tag) =>
		{
			var rand = randomArray[nativeThreadIndex];

			float2 randomVelocity = new float2(rand.NextFloat(-1.5f, 1.5f), rand.NextFloat(-1.5f, 1.5f));
			vel.Linear.xy = randomVelocity;
			vel.Angular.z = rand.NextFloat(-5, 5);
			ecb.RemoveComponent<AsteroidInitTag>(entityInQueryIndex, entity);
			randomArray[nativeThreadIndex] = rand;

		}).ScheduleParallel();

		_endSimulationEcbSystem.AddJobHandleForProducer(Dependency);
	}
}