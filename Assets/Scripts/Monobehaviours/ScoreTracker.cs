using Unity.Entities;
using UnityEngine;
using TMPro;

public class ScoreTracker : MonoBehaviour
{
	[SerializeField] GameObject _scoreTrackerPrefab;
	[SerializeField] TextMeshProUGUI _scoreDisplay;

	Entity _scoreTrackerEntity;
	Entity _scoreTrackerEntityInstance;
	ScoreData _scoreData;
	EntityManager _manager;
	BlobAssetStore _blobAssetStore;

    void Start()
    {
		_manager = World.DefaultGameObjectInjectionWorld.EntityManager;
		_blobAssetStore = new BlobAssetStore();
		GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, _blobAssetStore);
		_scoreTrackerEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(_scoreTrackerPrefab, settings);

		_scoreTrackerEntityInstance = _manager.Instantiate(_scoreTrackerEntity);
		_scoreData = _manager.GetComponentData<ScoreData>(_scoreTrackerEntityInstance);
	}

    // Update is called once per frame
    void Update()
    {
		_scoreData = _manager.GetComponentData<ScoreData>(_scoreTrackerEntityInstance);
		_scoreDisplay.text = $"Score: {_scoreData.Score}";
    }

	private void OnDestroy()
	{
		_blobAssetStore.Dispose();
	}
}
