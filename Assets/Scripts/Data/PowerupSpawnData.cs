using Unity.Entities;

[GenerateAuthoringComponent]
public struct PowerupSpawnData : IComponentData
{
	public Entity TwinShot;
	public Entity Shield;
	public Entity Drone;
	public Entity RapidFire;
}