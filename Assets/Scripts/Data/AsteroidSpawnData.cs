using Unity.Entities;

[GenerateAuthoringComponent]
public struct AsteroidSpawnData : IComponentData
{
	public Entity LargeAsteroid;
	public Entity MediumAsteroid;
	public Entity SmallAsteroid;

}
