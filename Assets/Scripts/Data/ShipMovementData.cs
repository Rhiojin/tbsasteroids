using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct ShipMovementData : IComponentData
{
	public float Acceleration;
	public float TurnSpeed;

	public bool RotateLeft;
	public bool RotateRight;
	public bool ThrusterOn;
	public bool HyperSpaceActive;
}
