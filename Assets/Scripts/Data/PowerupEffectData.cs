using Unity.Entities;

[GenerateAuthoringComponent]
public struct PowerupEffectData : IComponentData
{
	public Entity ShieldEffect;
	public Entity Drone;
}
