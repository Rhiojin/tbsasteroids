using Unity.Entities;

[GenerateAuthoringComponent]
public struct UfoSpawnData : IComponentData
{
	public Entity BasicUfo;
}