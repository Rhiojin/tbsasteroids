using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerShipSpawnData : IComponentData
{
	public Entity NormalShip;
}
