using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct ProjectileData : IComponentData
{
	public float Speed;
	public float3 Direction;
	public float TimeToLive;
}
