using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerPowerupStateData : IComponentData
{
	public bool TwinShot;
	public bool Shield;
	public bool Drone;
	public bool RapidFire;

	public float TwinShotTimer;
	public float ShieldTimer;
	public float DroneTimer;
	public float RapidFireTimer;

	public bool FlagDroneForCleanup;
	public bool FlagShieldForCleanup;
}