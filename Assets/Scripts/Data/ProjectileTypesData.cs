using Unity.Entities;

[GenerateAuthoringComponent]
public struct ProjectileTypesData : IComponentData
{
	public Entity BasicProjectile;
	public Entity RapidFireProjectile;
}