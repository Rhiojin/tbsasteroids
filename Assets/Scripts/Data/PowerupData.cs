using Unity.Entities;

[GenerateAuthoringComponent]
public struct PowerupData : IComponentData
{
	public int PowerupType;
}
