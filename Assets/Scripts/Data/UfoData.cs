using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct UfoData : IComponentData
{
	public float Speed;
	public float3 Direction;
}
