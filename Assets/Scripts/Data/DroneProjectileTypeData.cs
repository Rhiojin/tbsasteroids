using Unity.Entities;

[GenerateAuthoringComponent]
public struct DroneProjectileTypeData : IComponentData
{
	public Entity DroneProjectile;
}