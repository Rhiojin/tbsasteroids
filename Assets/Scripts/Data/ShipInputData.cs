using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct ShipInputData :IComponentData
{
	public KeyCode ThrustKey;
	public KeyCode LeftKey;
	public KeyCode RightKey;
	public KeyCode FireKey;
	public KeyCode HyperSpaceKey;

	public KeyCode ThrustKeyAlt;
	public KeyCode LeftKeyAlt;
	public KeyCode RightKeyAlt;
	public KeyCode FireKeyAlt;
	public KeyCode HyperSpaceKeyAlt;

	public float FireSpeed;
}