using Unity.Entities;

[GenerateAuthoringComponent]
public struct AsteroidTag : IComponentData
{
	public int AsteroidType;
}
